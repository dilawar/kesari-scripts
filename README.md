# Running the pipeline

This pipeline is best run in a terminal. It has been tested with centos-7 and 
Ubuntu-18.04/20.04. To run it on CentOS-7, you should install `cmake` version 
3.4 or higher. It is available in `epel` repositories. 


1. Install `cmake` and `tippecanoe`.
2. Go to this source directory and run `cmake .`
3. And wait for tiles to finish.

Extra layers can be added to `layers` directory. Make sure the extension is 
either `.json` or `geojson`.

Generated tiles can be served by `tileserver-gl`. Use docker for easy setup. 
