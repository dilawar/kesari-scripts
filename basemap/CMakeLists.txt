cmake_minimum_required(VERSION 3.1)
project(basemap LANGUAGES)

set(DOWNLOAD_URL https://download.geofabrik.de/asia/india-latest.osm.pbf)
set(OSM_FILE ${CMAKE_BINARY_DIR}/india-latest.osm.pbf)

add_custom_target(download_osm DEPENDS ${OSM_FILE})
add_custom_command(OUTPUT ${OSM_FILE}
  COMMAND wget ${DOWNLOAD_URL}
  COMMENT "Downloading ${OSM_FILE}"
  VERBATIM)

add_custom_target(basemap ALL)

# set(GDAL_DOCKER ${CMAKE_CURRENT_SOURCE_DIR}/gdal_docker.sh)
function(generate_layer _lname)
  set(OUTFILE ${CMAKE_CURRENT_SOURCE_DIR}/${_lname}.geojson)
  add_custom_target(${_lname} DEPENDS ${OUTFILE})
  add_custom_command(OUTPUT ${OUTFILE}
    COMMAND ogr2ogr -f GeoJSON ${OUTFILE} ${OSM_FILE} ${_lname}
    DEPENDS ${OSM_FILE}
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Generating layer ${_lname} in geojson format ${OUTFILE}."
    VERBATIM)
  add_dependencies(basemap ${_lname})
endfunction()

set(LAYERS points lines other_relations)
foreach(layer ${LAYERS})
  message(STATUS "Adding layer ${layer}")
  generate_layer(${layer})
endforeach()
