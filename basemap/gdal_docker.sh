#!/usr/bin/env bash
CMD="$1"
shift;
docker run --rm -v $(pwd):/data \
    klokantech/gdal ${CMD} "$@"
