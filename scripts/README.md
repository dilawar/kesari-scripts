1. Download OSM data and populate the PG database.
2. From this database, generate tiles.

# References 

1. https://github.com/gravitystorm/openstreetmap-carto/blob/master/INSTALL.md#scripted-download
